#ifndef PicFrame_H
#define PicFrame_H
#include <muduo/net/Buffer.h>
#include <muduo/base/Timestamp.h>
#include <string>
#include <memory>
#include <vector>

struct PicFrame;
typedef std::shared_ptr<PicFrame> PicFramePtr;


enum FrameCategory
{
    START = 0,
    PROCESSING = 1,
    END = 2
};

struct PicFrame
{
public:
    PicFrame(FrameCategory category, uint8_t picId, uint8_t packIdx)
        : category_(category),
          picId_(picId),
          packIdx_(packIdx),
          body_(),
          createTime_(muduo::Timestamp::now())
    {
    }

    PicFrame(FrameCategory category, uint8_t picId, uint8_t packIdx, const std::string& body)
        : category_(category),
          picId_(picId),
          packIdx_(packIdx),
          body_(body),
          createTime_(muduo::Timestamp::now())
    {
    }
    FrameCategory category_;
    uint8_t picId_;
    uint8_t packIdx_;
    std::string body_;
    muduo::Timestamp createTime_;
};


#endif // PicFrame_H
