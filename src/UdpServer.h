#ifndef UDPSERVER_H
#define UDPSERVER_H
#include <muduo/base/Timestamp.h>
#include <muduo/net/Buffer.h>
#include <functional>
#include <string>

extern "C" {
#include <unp.h>
}

class UdpServer;

typedef std::function<void (
        const UdpServer&,
        muduo::net::Buffer*,
        muduo::Timestamp)> MessageCallback;

class UdpServer
{
public:
    // UdpServer(int port);
    UdpServer(const std::string& ip, int port);
    void setRecvCallback(const MessageCallback& cb);
    void startListen();

private:
    std::string hostIp_;
    int port_;
    MessageCallback msgCb_;

    int udpFd_;
    int listenFd_;
    SA cliaddr_;
    muduo::net::Buffer recvBuf_;

};

#endif // UDPSERVER_H
