#ifndef PICMANAGER_H
#define PICMANAGER_H
#include "PicFrame.h"
#include <functional>
#include <algorithm>
#include <map>

typedef std::function<void (
        const std::string& path)> PicReadyFunctor;

class PicManager
{
public:
    explicit PicManager();

    void setPicReadyCallback(const PicReadyFunctor& cb);
    void signal_fileWritten(const std::string &name);

    void onReceiveFrame(PicFramePtr frame);

private:
    void saveImage(const std::vector<uint8_t> &data);

    PicReadyFunctor picReadyCb_;

    int currentPicId_ = -1;
    std::map<int, PicFramePtr> picMap_;
};

#endif // PICMANAGER_H
