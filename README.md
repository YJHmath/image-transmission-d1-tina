# 哪吒开发板实现TCP/UDP图传

## 协议

- 【腾讯文档】MCU图传到D1协议
  https://docs.qq.com/doc/DVkRZUXhSa3prbXZp

## 目标

- 运行在Tina-sdk
- TCP接受来自60000端口的数据
- UDP接收来自60001端口的数据
- jpeg图片输出到HDMI.
- 整合NCNN, 识别后输出.