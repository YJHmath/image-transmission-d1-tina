#!/bin/bash
riscv64-unknown-linux-gnu-gcc \
    -Os -pipe -mcmodel=medany -mabi=lp64d -march=rv64gcxthead -g3 \
    -fno-caller-saves \
    -Wno-format-truncation -Wno-unused-result  -Wformat -Werror=format-security \
    -Wl,-z,now -Wl,-z,relro -Wall \
    -c -o test.o test.c

riscv64-unknown-linux-gnu-gcc \
    -L/home/tao/tina-d1-open/out/d1-nezha/staging_dir/target/usr/lib  \
    -L/home/tao/tina-d1-open/out/d1-nezha/staging_dir/target/lib \
    -L/home/tao/tina-d1-open/prebuilt/gcc/linux-x86/riscv/toolchain-thead-glibc/riscv64-glibc-gcc-thead_20200702/usr/lib \
    -L/home/tao/tina-d1-open/prebuilt/gcc/linux-x86/riscv/toolchain-thead-glibc/riscv64-glibc-gcc-thead_20200702/lib \
    -znow \
    -zrelro \
    -L/home/tao/tina-d1-open/out/d1-nezha/staging_dir/target/usr/lib \
    -lpng -lz -lm \
    -o fbtest \
    test.o png.o fb_display.o transforms.o

# cp fbtest ~/AKE_EMBED/dev_root_nfs/tina/fbtest
