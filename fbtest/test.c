#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include "fbv.h"
#include <time.h>
//#include "fbviewer.h"

int main()
{
    printf("Hello world\n");
    // const char* path = "pic.jpg";
    const char* path = "image.png";
    uint32_t ts = 0;
    struct stat tmp;    
    stat(path, &tmp);
    //printf("st_atime:%d\n", tmp.st_atime);
    //printf("st_mtime:%d\n", tmp.st_mtime);
    printf("st_ctime:%d\n", tmp.st_ctime);
    
    //return 0;
    uint8_t* image = (uint8_t*)malloc(1600 * 1200 * 3);
    int is_need_output = 0;
    time_t rawTime;
    struct tm *timeinfo;
    while(1)
    {
        stat(path, &tmp);
        if(ts != tmp.st_ctime)
        {
            ts = tmp.st_ctime;
            is_need_output = 1;
        }
        if(is_need_output)
        {       
            fh_png_load(path, image, NULL, 1600, 1200);
            //fh_jpeg_load(path, image, NULL, 1600, 1200);
            fb_display(image, NULL, 1600, 1200, 0, 0, 160, 0);
            time(&rawTime);
            timeinfo = localtime(&rawTime);
            printf("[%s] display a pic\n", asctime(timeinfo));
            is_need_output = 0;
        }
        usleep(25 * 1000);
    }
    free(image);
    printf("finished.\n");
    getchar();
    return 0;
}
